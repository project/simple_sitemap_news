<?php

namespace Drupal\simple_sitemap_news\Plugin\simple_sitemap\SitemapGenerator;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\simple_sitemap\Plugin\simple_sitemap\SitemapGenerator\DefaultSitemapGenerator;
use Drupal\simple_sitemap\Plugin\simple_sitemap\SitemapGenerator\SitemapWriter;
use Drupal\Core\Database\Connection;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Component\Datetime\Time;

/**
 * NewsSitemapGenerator.
 *
 * @SitemapGenerator(
 *   id = "news",
 *   label = @Translation("News sitemap generator"),
 *   description = @Translation("Generates a news sitemap of your content."),
 * )
 */
class NewsSitemapGenerator extends DefaultSitemapGenerator {

  const XMLNS_NEWS = 'http://www.google.com/schemas/sitemap-news/0.9';

  /**
   * @var bool
   */
  protected $isHreflangSitemap;

  /**
   * @var array
   */
  protected static $attributes = [
    'xmlns' => self::XMLNS,
    'xmlns:xhtml' => self::XMLNS_XHTML,
    'xmlns:image' => self::XMLNS_IMAGE,
    'xmlns:news' => self::XMLNS_NEWS,
  ];

  /**
   * Adds attributes to the sitemap.
   */
  protected function addSitemapAttributes() {
    $attributes = self::$attributes;
    if (!$this->isHreflangSitemap()) {
      unset($attributes['xmlns:xhtml']);
    }
    $sitemap_variant = $this->sitemapVariant;
    $this->moduleHandler->alter('simple_sitemap_attributes', $attributes, $sitemap_variant);
    foreach ($attributes as $name => $value) {
      $this->writer->writeAttribute($name, $value);
    }
  }

  /**
   * Adds URL elements to the sitemap.
   *
   * @param array $links
   */
  protected function addLinks(array $links) {
    $sitemap_variant = $this->sitemapVariant;
    $this->moduleHandler->alter('simple_sitemap_links', $links, $sitemap_variant);
    foreach ($links as $url_data) {

      // Only include articles that were published in the last 2 days.
      $publishedAt = strtotime($url_data['news']['publication_date']);
      if ($publishedAt >= time() - 2 * 24 * 60 * 60) {

        // Create a new url element for every language
        if (!isset($url_data['alternate_urls'])) {
          $this->addUrl($url_data, $this->languageManager->getDefaultLanguage()->getId());
        } else {
          foreach ($url_data['alternate_urls'] as $alternate_url) {
            $url_data['url'] = $alternate_url;
            $this->addUrl($url_data);
          }
        }
      }
    }
  }

  /**
   * Adds a URL element to the sitemap.
   *
   * @param array $url_data
   *   The array of properties for this URL.
   */
  protected function addUrl(array $url_data) {
    $this->writer->startElement('url');

    $this->writer->writeElement('loc', $url_data['url']);
    // Add lastmod if any.
    if (isset($url_data['lastmod'])) {
      $this->writer->writeElement('lastmod', $url_data['lastmod']);
    }

    // Add changefreq if any.
    if (isset($url_data['changefreq'])) {
      $this->writer->writeElement('changefreq', $url_data['changefreq']);
    }

    // Add priority if any.
    if (isset($url_data['priority'])) {
      $this->writer->writeElement('priority', $url_data['priority']);
    }

    // Add news if any.
    if (isset($url_data['news'])) {
      $this->writer->startelement('news:news');
      if (isset($url_data['news']['publication_date'])) {
        $this->writer->writeElement('news:publication_date', $url_data['news']['publication_date']);
      }
      if (isset($url_data['news']['title'])) {
        $this->writer->writeElement('news:title', $url_data['news']['title']);
      }
      if (isset($url_data['news']['publication'])) {
        $this->writer->startelement('news:publication');
        if (isset($url_data['news']['publication']['name'])) {
          $this->writer->writeElement('news:name', $url_data['news']['publication']['name']);
        }
        $this->writer->endElement();
      }
      $this->writer->endElement();
    }

    // Add images if any.
    if (!empty($url_data['images'])) {
      foreach ($url_data['images'] as $image) {
        $this->writer->startElement('image:image');
        $this->writer->writeElement('image:loc', $image['path']);
        if (strlen($image['title']) > 0) {
          $this->writer->writeElement('image:title', $image['title']);
        }
        if (strlen($image['alt']) > 0) {
          $this->writer->writeElement('image:caption', $image['alt']);
        }
        $this->writer->endElement();
      }
    }

    $this->writer->endElement();
  }

}
