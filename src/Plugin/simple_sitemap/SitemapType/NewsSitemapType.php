<?php

namespace Drupal\simple_sitemap_news\Plugin\simple_sitemap\SitemapType;

use Drupal\simple_sitemap\Plugin\simple_sitemap\SitemapType\SitemapTypeBase;

/**
 * NewsSitemapType.
 *
 * @SitemapType(
 *   id = "news",
 *   label = @Translation("News"),
 *   description = @Translation("A news sitemap type."),
 *   sitemapGenerator = "news",
 *   urlGenerators = {
 *     "custom",
 *     "news_entity",
 *     "entity_menu_link_content",
 *     "arbitrary",
 *   },
 * )
 */
class NewsSitemapType extends SitemapTypeBase {
}
