<?php

namespace Drupal\simple_sitemap_news\Plugin\simple_sitemap\UrlGenerator;

use Drupal\simple_sitemap\Plugin\simple_sitemap\UrlGenerator\EntityUrlGenerator;

/**
 * NewsEntityUrlGenerator.
 *
 * Generates URLs for news entity bundles and bundle overrides.
 *
 * @UrlGenerator(
 *   id = "news_entity",
 *   label = @Translation("News URL generator"),
 *   description = @Translation("Generates URLs for news entity bundles and bundle overrides."),
 * )
 */
class NewsEntityUrlGenerator extends EntityUrlGenerator {

  /**
   * {@inheritdoc}
   */
  protected function processDataSet($data_set) {
    $processedData = parent::processDataSet($data_set);
    if (!$processedData) {
      return $processedData;
    }

    if (empty($entity = $this->entityTypeManager->getStorage($data_set['entity_type'])->load($data_set['id']))) {
      return FALSE;
    }

    $processedData['news'] = [
      'publication' => [
        'name' => \Drupal::config('system.site')->get('name'),
        'language' => method_exists($entity, 'language') ? $entity->language()->getId : NULL,
      ],
      'publication_date' => method_exists($entity, 'getCreatedTime') ? date('c', $entity->getCreatedTime()) : NULL,
      'title' => method_exists($entity, 'getTitle') ? $entity->getTitle() : NULL,
    ];
    return $processedData;
  }

}
